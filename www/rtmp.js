var exec = require('cordova/exec');

function rtmp() { 
 console.log("plugin rtmp.js: has been created");
}

rtmp.prototype.init = function(success, fail){
 console.log("rtmp.js: Plugin initialized.");

 exec(success, fail, "rtmp", "init",[]);

};

rtmp.prototype.toggleStreaming = function(success, fail){
 exec(success, fail, "rtmp", "toggleStreaming",[]);
};

var p = new rtmp();
module.exports = p;