package com.vbandrew.rtmp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wowza.gocoder.sdk.api.WowzaGoCoder;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcast;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcastConfig;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.devices.WZAudioDevice;
import com.wowza.gocoder.sdk.api.devices.WZCameraView;
import com.wowza.gocoder.sdk.api.devices.WZCamera;
import com.wowza.gocoder.sdk.api.errors.WZError;
import com.wowza.gocoder.sdk.api.errors.WZStreamingError;
import com.wowza.gocoder.sdk.api.status.WZState;
import com.wowza.gocoder.sdk.api.status.WZStatus;
import com.wowza.gocoder.sdk.api.status.WZStatusCallback;

import java.util.UUID;

/**
 * Created by root on 09/02/17.
 */

public class cameraActivity extends AppCompatActivity
        implements WZStatusCallback, View.OnClickListener {


    private static String TAG = "cameraActivity";

    // The top level GoCoder API interface
    private WowzaGoCoder goCoder;

    // The GoCoder SDK camera view
    private WZCameraView goCoderCameraView;

    private WZCamera goCoderCamera;

    // The GoCoder SDK audio device
    private WZAudioDevice goCoderAudioDevice;

    // The GoCoder SDK broadcaster
    private WZBroadcast goCoderBroadcaster;

    // The broadcast configuration settings
    private WZBroadcastConfig goCoderBroadcastConfig;

    private String transmision = UUID.randomUUID().toString().substring(0,5);

    // Properties needed for Android 6+ permissions handling
    private static final int PERMISSIONS_REQUEST_CODE = 0x1;
    private boolean mPermissionsGranted = true;
    private String[] mRequiredPermissions = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        /*setContentView(R.layout.activity_main);
        TextView etiqueta = (TextView) findViewById(R.id.textView);
        etiqueta.setText("Tu id es: " + transmision);*/

        //int main = this.getResources().getIdentifier("activity_main", "layout", this.getPackageName());
        int etiquetaId  = this.getResources().getIdentifier("textView", "id", this.getPackageName());
        //cameraPreviewInt = this.getResources().getIdentifier("camera_preview", "id", this.getPackageName());

        //setContentView(R.layout.activity_main);//<--------------------------------
        setContentView(this.getResources().getIdentifier("activity_main", "layout", this.getPackageName()));
        TextView etiqueta = (TextView) findViewById(etiquetaId);
        etiqueta.setText("Tu id es: " + transmision);


        // Initialize the GoCoder SDK
        //goCoder = WowzaGoCoder.init(getApplicationContext(), "GOSK-5243-0103-8BC3-933D-9349");//mia
        goCoder = WowzaGoCoder.init(getApplicationContext(), "GOSK-4D43-0103-4172-069B-C651");//Juan

        if (goCoder == null) {
            // If initialization failed, retrieve the last error and display it
            WZError goCoderInitError = WowzaGoCoder.getLastError();
            Toast.makeText(this,
                    "GoCoder SDK error: " + goCoderInitError.getErrorDescription(),
                    Toast.LENGTH_LONG).show();
            return;
        }


         Log.d(TAG, "AAAAAAAAAAAAAAAAAA");

        // Associate the WZCameraView defined in the U/I layout with the corresponding class member
        //goCoderCameraView = (WZCameraView) findViewById(R.id.camera_preview);//<--------------------------------
        goCoderCameraView = (WZCameraView) findViewById(this.getResources().getIdentifier("camera_preview", "id", this.getPackageName()));

        Log.d(TAG, "BBBBBBBBBBBBBBBBBB");

        // Create an audio device instance for capturing and broadcasting audio
        goCoderAudioDevice = new WZAudioDevice();

        Log.d(TAG, "CCCCCCCCCCCCCCCCCC");

        // Create a broadcaster instance
        goCoderBroadcaster = new WZBroadcast();

        Log.d(TAG, "DDDDDDDDDDDDDDDDDD");

        // Create a configuration instance for the broadcaster
        //goCoderBroadcastConfig = new WZBroadcastConfig(WZMediaConfig.FRAME_SIZE_1920x1080);
        goCoderBroadcastConfig = new WZBroadcastConfig(WZMediaConfig.FRAME_SIZE_1280x720);

        Log.d(TAG, "EEEEEEEEEEEEEEEEEE");

        // Set the connection properties for the target Wowza Streaming Engine server or Wowza Cloud account 
        goCoderBroadcastConfig.setHostAddress("eonproduccion.net");
        //goCoderBroadcastConfig.setHostAddress("rtsp://345190.entrypoint.cloud.wowza.com/app-4325");
        goCoderBroadcastConfig.setPortNumber(1935);
        goCoderBroadcastConfig.setApplicationName("live");
        goCoderBroadcastConfig.setStreamName(transmision);
        //goCoderBroadcastConfig.setStreamName("d3b2a5ba");
        
        goCoderBroadcastConfig.setUsername("client19072");
        goCoderBroadcastConfig.setPassword("bde7bf1c");

        Log.d(TAG, "FFFFFFFFFFFFFFFFF");

        // Designate the camera preview as the video broadcaster
        goCoderBroadcastConfig.setVideoBroadcaster(goCoderCameraView);

        Log.d(TAG, "GGGGGGGGGGGGGGGGGG");

        // Designate the audio device as the audio broadcaster
        goCoderBroadcastConfig.setAudioBroadcaster(goCoderAudioDevice);

        Log.d(TAG, "HHHHHHHHHHHHHHHHHH");

        // Associate the onClick() method as the callback for the broadcast button's click event
        //Button broadcastButton = (Button) findViewById(R.id.broadcast_button);//<--------------------------------
        Button broadcastButton = (Button) findViewById(this.getResources().getIdentifier("broadcast_button", "id", this.getPackageName()));
        broadcastButton.setOnClickListener(this);

        //Button cancelButton = (Button) findViewById(R.id.close_btn);//<--------------------------------
        Button cancelButton = (Button) findViewById(this.getResources().getIdentifier("close_btn", "id", this.getPackageName()));
        cancelButton.setOnClickListener(this);
    }

    //
    // Called when an activity is brought to the foreground
    //
    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "**************** ON RESUME!!!!!! *****************");

        // If running on Android 6 (Marshmallow) or above, check to see if the necessary permissions
        // have been granted
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPermissionsGranted = hasPermissions(this, mRequiredPermissions);
            if (!mPermissionsGranted)
                ActivityCompat.requestPermissions(this, mRequiredPermissions, PERMISSIONS_REQUEST_CODE);
        } else
            mPermissionsGranted = true;

        // Start the camera preview display
        if (mPermissionsGranted && goCoderCameraView != null) {
            if (goCoderCameraView.isPreviewPaused()){
                Log.d(TAG, "**************** isPreviewPaused() *****************");
                goCoderCameraView.onResume();
            }
            else{
                Log.d(TAG, "================ !isPreviewPaused() =================");
                goCoderCameraView.startPreview();
            }
        }

    }

    //
    // Callback invoked in response to a call to ActivityCompat.requestPermissions() to interpret
    // the results of the permissions request
    //
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        mPermissionsGranted = true;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                // Check the result of each permission granted
                for(int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        mPermissionsGranted = false;
                    }
                }
            }
        }
    }

    //
    // Utility method to check the status of a permissions request for an array of permission identifiers
    //
    private static boolean hasPermissions(Context context, String[] permissions) {
        for(String permission : permissions)
            if (context.checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                return false;

        return true;
    }

    //
    // The callback invoked when the broadcast button is pressed
    //
    @Override
    public void onClick(View v) {
        //if(v.getId() == R.id.broadcast_button)//<--------------------------------
        if(v.getId() == this.getResources().getIdentifier("broadcast_button", "id", this.getPackageName()))
            broadcast();
        //else if(v.getId() == R.id.close_btn)//<--------------------------------
        else if(v.getId() == this.getResources().getIdentifier("close_btn", "id", this.getPackageName()))
            closeApp();
    }

    public void broadcast(){
        // return if the user hasn't granted the app the necessary permissions
        if (!mPermissionsGranted) return;

        // Ensure the minimum set of configuration settings have been specified necessary to
        // initiate a broadcast streaming session
        WZStreamingError configValidationError = goCoderBroadcastConfig.validateForBroadcast();

        if (configValidationError != null) {
            Toast.makeText(this, configValidationError.getErrorDescription(), Toast.LENGTH_LONG).show();
            Intent intent = new Intent();
            setResult(Activity.RESULT_CANCELED, intent);
            Log.e("rtmp", "configValidationError");
            finish();
        } else if (goCoderBroadcaster.getStatus().isRunning()) {
            // Stop the broadcast that is currently running
            goCoderBroadcaster.endBroadcast(this);
            /*Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            Log.e("rtmp", "result ok");
            finish();*/
        } else {
            // Start streaming
            goCoderBroadcaster.startBroadcast(goCoderBroadcastConfig, this);
        }
    }

    public void closeApp(){
        goCoderCameraView.stopPreview();

        //goCoderCamera.release();

        //Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, null);
        finish();
    }

    //
    // The callback invoked upon changes to the state of the steaming broadcast
    //
    @Override
    public void onWZStatus(final WZStatus goCoderStatus) {
        // A successful status transition has been reported by the GoCoder SDK
        final StringBuffer statusMessage = new StringBuffer("Broadcast status: ");

        switch (goCoderStatus.getState()) {
            case WZState.STARTING:
                statusMessage.append("Broadcast initialization");
                break;

            case WZState.READY:
                statusMessage.append("Ready to begin streaming");
                break;

            case WZState.RUNNING:
                statusMessage.append("Streaming is active");
                break;

            case WZState.STOPPING:
                statusMessage.append("Broadcast shutting down");
                break;

            case WZState.IDLE:
                statusMessage.append("The broadcast is stopped");
                break;

            default:
                return;
        }

        // Display the status message using the U/I thread
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(cameraActivity.this, statusMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    //
    // The callback invoked when an error occurs during a broadcast
    //
    @Override
    public void onWZError(final WZStatus goCoderStatus) {
        // If an error is reported by the GoCoder SDK, display a message
        // containing the error details using the U/I thread
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(cameraActivity.this,
                        "Streaming error: " + goCoderStatus.getLastError().getErrorDescription(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    //
    // Enable Android's sticky immersive full-screen mode
    //
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        if (rootView != null)
            rootView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
