package com.vbandrew.rtmp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore;

//import java.util.ArrayList;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.LOG;


import org.json.JSONArray;
import org.json.JSONException;



public class rtmp extends CordovaPlugin {
    private static String LOG_TAG = "rtmp_MainActivity";
    private static int CAMERA = 1;


    CallbackContext callbackContext;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    /*@Override
    protected void onResume(){
        super.onResume();
        LOG.d(LOG_TAG, "Xxxxxxxxxx ON RESUME xxxxxxxxxxxX");
    }*/

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;

        /*ArrayList<String> exts = new ArrayList<String>();

        int count = args.length();

        for(int i = 0;i<count;i++)
        {
            exts.add(args.getString(i).toLowerCase());
        }*/

        if (action.equals("toggleStreaming")) {
            //startStreaming(callbackContext, exts);
            startStreaming(callbackContext);
            return true;
        }

        return false;
    }

    private void startStreaming(CallbackContext callbackCtx){//, ArrayList exts){
        LOG.d(LOG_TAG, "Dentro de startStreaming()");

        callbackContext = callbackCtx;

        if (this.cordova != null) {

            // Let's check to make sure the camera is actually installed. (Legacy Nexus 7 code)
            PackageManager mPm = this.cordova.getActivity().getPackageManager();


            Context context=this.cordova.getActivity().getApplicationContext();

            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.setClass(context, cameraActivity.class);



            if(intent.resolveActivity(mPm) != null){

                cordova.startActivityForResult((CordovaPlugin) this, intent, CAMERA);

                //callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
                callbackContext.success("OK");
            }
            else
            {
                LOG.e(LOG_TAG, "Error: You don't have a default camera.  Your device may not be CTS complaint.");
                callbackContext.error("You don't have a default camera.  Your device may not be CTS complaint.");
            }
        }
        else{
            LOG.e(LOG_TAG, "Cordova no esta disponible.");
            callbackContext.error("Cordova no esta disponible.");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if (requestCode == CAMERA && callbackContext != null) {

            if (resultCode == Activity.RESULT_OK) {
                LOG.d(LOG_TAG, "*************** Activity.RESULT_OK");
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
                //callbackContext.success("OK");
            }

        } else if (resultCode == Activity.RESULT_CANCELED) {
            LOG.d(LOG_TAG, "*************** Activity.RESULT_CANCELED");
            // TODO NO_RESULT or error callback?
            //PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            //PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));

        } else {
            LOG.d(LOG_TAG, "*X*X*X*X*X*X*X*X* ERROR, resultCode == "+resultCode);
            callbackContext.error(resultCode);
        }
    }

}